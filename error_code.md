|Code|Error|
|----|-----|
|1000|Validation error|
|1001|Email already exists|
|1002|Not found|
|1003|Password does not match|
|1004|You are not the owner of the product|
|1005|You have added this product to your wishlist|
|1006|Token does not match or expired|
|1007|Your email address is verified|
|1013|Permission Denied|
|1014|Bạn không phải là khách hàng|
|1015|Bạn không phải là luật sư|
|1016|Yêu cầu này đã hoàn thành nên không thể thêm bình luận|
|1017|Yêu cầu này chưa được luật sư chấp nhận|
|1018|Yêu cầu này chưa hoàn thành|
