import 'jquery';
import 'bootstrap';
import 'select2/dist/js/select2.min.js';
import 'jquery-bar-rating/dist/jquery.barrating.min.js';
import 'autoresize-textarea/dist/autoresize-textarea.js';

import 'moment';
import 'eonasdan-bootstrap-datetimepicker';

import './imports/custom.js';