jQuery(document).ready(function($) {
    //form search header
    $('body').click(function(event) {
        $(this).find('.search-box form input').fadeOut();
    });
    $('.search-box form .search-icon').click(function(event) {
        $(this).parent().find('input').fadeToggle();
        event.stopPropagation(); //ko tính click body
    });
    $('.search-box form input').click(function(event) {
        $(this).fadeIn();
        event.stopPropagation();
    });

    // Home slider
    $('.slider').slick({
        dots: true,
        slidesToShow: 1,
        arrows: true,
        autoplay: true,
        responsive: [{
            breakpoint: 992,
            settings: {
                arrows: false,
                centerPadding: '10px',
                slidesToShow: 1
            }
        }]
    });

    // product slider
    $('.product-slider .product-slider-list').slick({
        dots: false,
        slidesToShow: 5,
        slidesToScroll: 1,
        arrows: true,
        responsive: [{
                breakpoint: 992,
                settings: {
                    arrows: false,
                    centerPadding: '10px',
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerPadding: '10px',
                    slidesToShow: 2
                }
            }
        ]
    });

    // Sale tea slider
    $('.sale-tea-content').slick({
        dots: false,
        slidesToShow: 2,
        slidesToScroll: 1,
        arrows: true,
        responsive: [{
            breakpoint: 480,
            settings: {
                arrows: false,
                centerPadding: '10px',
                slidesToShow: 1
            }
        }]
    });

    // Single product active
    $('.pro-size ul li').click(function() {
        $('.pro-size ul').find('.active').removeClass("active");
        $(this).addClass("active");
    });
    $('.pro-ice ul li').click(function() {
        $('.pro-ice ul').find('.ice-active').removeClass("ice-active");
        $(this).addClass("ice-active");
    });
    $('.pro-sugar ul li').click(function() {
        $('.pro-sugar ul').find('.ice-active').removeClass("ice-active");
        $(this).addClass("ice-active");
    });

    //menu
    $('.main-menu').meanmenu({
        meanScreenWidth: "1200",
        meanMenuContainer: ".mobile-menu",
    });

});