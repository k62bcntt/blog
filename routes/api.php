<?php

$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api) {
    $api->resource('test', 'App\Http\Controllers\TestController');
    $api->post('file/upload', 'App\Http\Controllers\Api\FileController@upload');

    // Forgot password
    $api->post('password/email', 'App\Http\Controllers\Auth\ForgotPasswordController@sendResetLinkEmail');
    $api->post('password/reset', ['as' => 'password.reset', 'uses' => 'App\Http\Controllers\Auth\ResetPasswordController@reset']);
    $api->get('password/check-reset-password-token', 'App\Http\Controllers\Auth\ResetPasswordController@checkResetPasswordToken');

    // Users
    $api->get('min-max-experience-salary', 'App\Http\Controllers\Api\Frontend\UserController@minMaxExperienceSalary');
    $api->put('users/{id}/images', 'App\Http\Controllers\Api\Frontend\UserController@images');
    $api->put('users/change-password', 'App\Http\Controllers\Api\Frontend\UserController@changePassword');
    $api->get('users/all', 'App\Http\Controllers\Api\Frontend\UserController@getAll');
    $api->get('users', 'App\Http\Controllers\Api\Frontend\UserController@index');
    $api->get('users/{id}', 'App\Http\Controllers\Api\Frontend\UserController@show');
    $api->put('users/{id}', 'App\Http\Controllers\Api\Frontend\UserController@update');
    $api->get('users/{id}/is-verified-email', 'App\Http\Controllers\Api\Frontend\UserController@isVerifyEmail');
    $api->put('users/{id}/verify-email', 'App\Http\Controllers\Api\Frontend\UserController@verifyEmail');
    $api->put('users/{id}/resend-verify-email', 'App\Http\Controllers\Api\Frontend\UserController@resendVerifyEmail');
    $api->put('users/{id}/role', 'App\Http\Controllers\Api\Frontend\UserController@updateRole');
    $api->put('users/{id}/profile-update', 'App\Http\Controllers\Api\Frontend\UserController@profileUpdate');

    // Products
    $api->get('products/all', 'App\Http\Controllers\Api\Frontend\ProductController@getAll');
    $api->get('products/slug/{slug}', 'App\Http\Controllers\Api\Frontend\ProductController@getProductBySlug');
    $api->resource('products', 'App\Http\Controllers\Api\Frontend\ProductController');

    $api->group(['prefix' => 'admin'], function ($api) {
        $api->post('login', 'App\Http\Controllers\Api\Admin\AuthController@authenticate');
        $api->get('me', 'App\Http\Controllers\Api\Admin\AuthController@me');

        // Users
        $api->put('users/{id}/images', 'App\Http\Controllers\Api\Admin\UserController@images');
        $api->get('users/all', 'App\Http\Controllers\Api\Admin\UserController@getAll');
        $api->put('users/change-status-item/{id}', 'App\Http\Controllers\Api\Admin\UserController@changeStatusItem');
        $api->put('users/change-status-all-items', 'App\Http\Controllers\Api\Admin\UserController@changeStatusAllItems');
        $api->resource('users', 'App\Http\Controllers\Api\Admin\UserController');

        // Stores
        $api->get('stores/all', 'App\Http\Controllers\Api\Admin\StoreController@getAll');
        $api->put('stores/change-status-item/{id}', 'App\Http\Controllers\Api\Admin\StoreController@changeStatusItem');
        $api->put('stores/change-status-all-items', 'App\Http\Controllers\Api\Admin\StoreController@changeStatusAllItems');
        $api->resource('stores', 'App\Http\Controllers\Api\Admin\StoreController');

        // Products
        $api->get('products/all', 'App\Http\Controllers\Api\Admin\ProductController@getAll');
        $api->put('products/change-status-item/{id}', 'App\Http\Controllers\Api\Admin\ProductController@changeStatusItem');
        $api->put('products/change-status-all-items', 'App\Http\Controllers\Api\Admin\ProductController@changeStatusAllItems');
        $api->resource('products', 'App\Http\Controllers\Api\Admin\ProductController');

        // Toppings
        $api->get('toppings/all', 'App\Http\Controllers\Api\Admin\ToppingController@getAll');
        $api->put('toppings/change-status-item/{id}', 'App\Http\Controllers\Api\Admin\ToppingController@changeStatusItem');
        $api->put('toppings/change-status-all-items', 'App\Http\Controllers\Api\Admin\ToppingController@changeStatusAllItems');
        $api->resource('toppings', 'App\Http\Controllers\Api\Admin\ToppingController');

        // Categories
        $api->get('categories/all', 'App\Http\Controllers\Api\Admin\CategoryController@getAll');
        $api->put('categories/change-status-item/{id}', 'App\Http\Controllers\Api\Admin\CategoryController@changeStatusItem');
        $api->put('categories/change-status-all-items', 'App\Http\Controllers\Api\Admin\CategoryController@changeStatusAllItems');
        $api->resource('categories', 'App\Http\Controllers\Api\Admin\CategoryController');

        // Posts
        $api->get('posts/all', 'App\Http\Controllers\Api\Admin\PostController@getAll');
        $api->put('posts/change-status-item/{id}', 'App\Http\Controllers\Api\Admin\PostController@changeStatusItem');
        $api->put('posts/change-status-all-items', 'App\Http\Controllers\Api\Admin\PostController@changeStatusAllItems');
        $api->resource('posts', 'App\Http\Controllers\Api\Admin\PostController');
    });
});
