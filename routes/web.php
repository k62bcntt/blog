<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('', function() {
    return view('pages.home');
});
Route::get('test', 'TestController@index');

/**
 * Api Route
 */
Route::post('api/login', 'Api\Frontend\AuthController@authenticate');
Route::post('api/social-login', 'Api\Frontend\AuthController@socialLogin');
Route::post('api/users', 'Api\Frontend\UserController@store');
Route::get('api/me', 'Api\Frontend\AuthController@me');
/**
 * End Api Route
 */

Route::post('logout', 'Auth\AuthController@logout')->name('logout');

Route::any('admin/{path?}', function () {
    return view('spa.admin');
})->where("path", ".+");

// Route::any('{path?}', function () {
//     return view('spa.app');
// })->where("path", ".+");
