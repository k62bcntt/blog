<?php

use App\Entities\User;
use Illuminate\Database\Seeder;
use NF\Roles\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = new Faker\Generator();
        $faker->addProvider(new Faker\Provider\en_US\Person($faker));
        $faker->addProvider(new Faker\Provider\en_SG\Address($faker));
        $faker->addProvider(new Faker\Provider\en_US\Company($faker));
        $faker->addProvider(new Faker\Provider\DateTime($faker));
        $faker->addProvider(new Faker\Provider\Internet($faker));
        $faker->addProvider(new Faker\Provider\Lorem($faker));

        $description = '';
        foreach ($faker->paragraphs() as $paragraph) {
            $description .= $paragraph;
        }

        $adminRole   = Role::find(1);
        $managerRole = Role::find(2);
        $userRole    = Role::find(3);

        $users = [
            'admin'   => [
                'first_name' => 'Admin',
                'last_name'  => '',
                'email'      => 'admin@leetee.com',
                'role'       => 'adminRole',
                'active'     => 1,
            ],
            'manager' => [
                'first_name' => 'Manager',
                'last_name'  => '',
                'email'      => 'manager@leetee.com',
                'role'       => 'managerRole',
                'active'     => 1,
            ],
            'user'    => [
                'first_name' => 'User',
                'last_name'  => '',
                'email'      => 'user@leetee.com',
                'role'       => 'userRole',
                'active'     => 1,
            ],
        ];

        foreach ($users as $item) {
            $user_data = [
                'email'      => $item['email'],
                'first_name' => $item['first_name'],
                'last_name'  => $item['last_name'],
            ];

            $user = User::updateOrCreate([
                'email' => $item['email'],
            ], $user_data);

            $user->password = Hash::make('secret');
            $user->avatar   = '/uploads/default_avatar.png';
            $user->status   = User::STATUS_ACTIVE;
            $user->save();
            $user->attachRole(${$item['role']});
        }

        for ($i = 0; $i < 20; $i++) {
            if ($i % 2 == 0) {
                $gender = 'male';
            } else {
                $gender = 'female';
            }
            $email = $faker->email();

            $user_data = [
                'email'      => $email,
                'first_name' => $faker->firstName($gender),
                'last_name'  => $faker->lastName($gender),
            ];

            $user = User::updateOrCreate([
                'email' => $email,
            ], $user_data);

            $user->password = Hash::make('secret');
            $user->avatar   = '/uploads/default_avatar.png';
            $user->status   = User::STATUS_ACTIVE;
            $user->save();
            if ($i % 3 == 0) {
                $user->attachRole($managerRole);
            } else {
                $user->attachRole($userRole);
            }
        }
    }
}
