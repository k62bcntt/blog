<?php

use App\Entities\Store;
use App\Entities\User;
use Illuminate\Database\Seeder;

class StoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = new Faker\Generator();
        $faker->addProvider(new Faker\Provider\en_US\Person($faker));
        $faker->addProvider(new Faker\Provider\en_SG\Address($faker));
        $faker->addProvider(new Faker\Provider\en_US\Company($faker));
        $faker->addProvider(new Faker\Provider\DateTime($faker));
        $faker->addProvider(new Faker\Provider\Internet($faker));
        $faker->addProvider(new Faker\Provider\Lorem($faker));

        $manager = User::whereHas('roles', function ($q) {
            $q->where('slug', 'manager');
        })->first();

        if ($manager) {
            for ($i = 0; $i < 20; $i++) {

                $data = [
                    'name'    => $faker->company,
                    'address' => $faker->address,
                ];

                $store          = new Store;
                $store->name    = $data['name'];
                $store->address = $data['address'];
                $store->status  = Store::STATUS_ACTIVE;
                $store->user()->associate($manager);
                $store->save();
            }
        }
    }
}
