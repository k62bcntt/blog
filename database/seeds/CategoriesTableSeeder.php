<?php

use App\Entities\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'name' => 'Mango cheese tea',
            ],
            [
                'name' => 'Cheese tea',
            ],
        ];

        foreach ($categories as $item) {
            $item['slug']     = str_slug($item['name']);
            $category         = Category::updateOrCreate(['name' => $item['name']], $item);
            $category->status = Category::STATUS_ACTIVE;
            $category->save();
        }
    }
}
