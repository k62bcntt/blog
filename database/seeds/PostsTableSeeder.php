<?php
use App\Entities\Category;
use App\Entities\Post;
use App\Entities\User;
use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = new Faker\Generator();
        $faker->addProvider(new Faker\Provider\en_US\Person($faker));
        $faker->addProvider(new Faker\Provider\en_SG\Address($faker));
        $faker->addProvider(new Faker\Provider\en_US\Company($faker));
        $faker->addProvider(new Faker\Provider\DateTime($faker));
        $faker->addProvider(new Faker\Provider\Internet($faker));
        $faker->addProvider(new Faker\Provider\Lorem($faker));

        $admin = User::where('email', 'admin@uplaw.com')->first();

        $categories_child_ids = Category::where('parent_id', '!=', 0)->get()->pluck('id');
        $min_id               = $categories_child_ids->min();
        $max_id               = $categories_child_ids->max();

        if ($admin) {
            for ($i = 0; $i < 20; $i++) {
                $title = $faker->sentence($nbWords = 6, $variableNbWords = true);

                $data = [
                    'title'   => $title,
                    'slug'    => str_slug($title),
                    'image'   => '/uploads/default_news.jpg',
                    'content' => $faker->paragraphs($nb = 30, $asText = true),
                    'user_id' => $admin->id,
                    'type'    => Post::TYPE_POST,
                ];

                $post         = Post::create($data);
                $post->status = Post::STATUS_ACTIVE;
                $post->save();

                $post->categories()->sync(mt_rand($min_id, $max_id));
            }

            for ($i = 0; $i < 20; $i++) {
                if ($i % 2 == 0) {
                    $gender = 'male';
                } else {
                    $gender = 'female';
                }

                $title = $faker->sentence($nbWords = 6, $variableNbWords = true);

                $data = [
                    'title'   => $title,
                    'slug'    => str_slug($title),
                    'image'   => '/uploads/default_news.jpg',
                    'content' => $faker->paragraphs($nb = 30, $asText = true),
                    'lawyer'  => "{$faker->firstName($gender)} {$faker->lastName($gender)}",
                    'answer'  => $faker->paragraphs($nb = 30, $asText = true),
                    'user_id' => $admin->id,
                    'type'    => Post::TYPE_LEGAL_BLOG,
                ];

                $post         = Post::create($data);
                $post->status = Post::STATUS_ACTIVE;
                $post->save();

                $post->categories()->sync(mt_rand($min_id, $max_id));
            }
        }
    }
}
