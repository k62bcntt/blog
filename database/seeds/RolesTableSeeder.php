<?php

use Illuminate\Database\Seeder;
use NF\Roles\Models\Permission;
use NF\Roles\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::updateOrCreate([
            'name' => 'Admin',
            'slug' => 'admin',
        ]);

        $manager = Role::updateOrCreate([
            'name' => 'Manager',
            'slug' => 'manager',
        ]);

        $user = Role::updateOrCreate([
            'name' => 'User',
            'slug' => 'user',
        ]);
    }
}
