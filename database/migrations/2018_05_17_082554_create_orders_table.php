<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('store_id')->unsigned();
            $table->foreign('store_id')->references('id')->on('stores')->onDelete('cascade');
            $table->integer('coupon_id')->unsigned();
            $table->foreign('coupon_id')->references('id')->on('coupons')->onDelete('cascade');
            $table->integer('shipping_id')->unsigned();
            $table->foreign('shipping_id')->references('id')->on('shippings')->onDelete('cascade');
            $table->integer('total_price');
            $table->integer('shipping_fee');
            $table->integer('user_id');
            $table->string('user_fullname', 255);
            $table->string('user_email', 150);
            $table->string('user_phone_number', 20);
            $table->string('address', 250);
            $table->text('note');
            $table->integer('status')->comment('0: pending; 1: active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
