@component('mail::message')
Please click <a href="{{ url('reset-password', $token) }}">here</a> to reset password

<p>&nbsp;</p>
Thanks,<br>
{{ config('app.name') }}
@endcomponent
