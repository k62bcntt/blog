@extends('layout.master')

@section('title')
Uplaw | {!! $new->title !!}
@endsection

@section('og_title')
Uplaw | {!! $new->title !!}
@endsection

@section('og_description')
{!! str_limit($new->content, 500) !!}
@endsection

@section('content')
<div class="legalblog-show-page">
	<div class="top-marker">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 top-marker-wrapper flex-box flex-items-center">
					<h1>News</h1>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
			@include('partials.legalblog-right-column')
			
			<div class="col-xs-12 col-sm-9 col-sm-pull-3 legalblog-content">
				<div class="legalblog-content-wrapper">
					<div class="legalblog-question">
						<h3>{!! $new->title !!}</h3>
						<p class="date">Ngày đăng: {!! date('d/m/Y', strtotime($new->updated_at)) !!}</p>
						<p class="question">{!! $new->content !!}</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection