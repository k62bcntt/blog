@extends('layout.master')

@section('content')
<section class="cart">
	<nav class="breadcrumbs">
		<div class="container">
			<span>
				<a href="<?php echo site_url('index.php'); ?>">Home</a> > 
				<a href="<?php echo site_url('index.php'); ?>">New</a> >
				<span class="breadcrumb_last">My Bag</span>
			</span>
		</div>
	</nav>
	<div class="container">
		<div class="section-title">
			<h1>My Bag</h1>
		</div>
		<div class="cart-table">
			<div class="table-cart">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Hình ảnh</th>
							<th>Tên sản phẩm</th>
							<th>Size</th>
							<th>Món thêm</th>
							<th>Số lượng</th>
							<th>Đơn giá</th>
							<th>Tổng</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<img src="<?php echo asset_image('cart-image.png'); ?>" alt="">
							</td>
							<td>Fresh juice tea ( I70% S30%)</td>
							<td>M</td>
							<td>Thạch trà xanh , Kem chese</td>
							<td>
								<div class="quality">
									<span class="plus" onClick="Tang()">+</span>
									<input type="number" name="quality" id="quality" value="2">
									<span class="minus" id="tru" onClick="Giam()">-</span>
								</div>

							</td>
							<td>48.000 VND</td>
							<td>96.000 VND</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="row">
				<div class="col-xl-7 col-lg-7 col-md-7 col-sm-12 col-12">
					<div class="note">
						<label for="note">Ghi chú</label>
						<textarea name="note" id="note"></textarea>
					</div>
				</div>
				<div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12 cart-total">
					<span class="total">Tổng cộng: 96.000 VND</span>
					<a href="" class="cart-button">Tiếp tục mua hàng</a>
					<a href="" class="cart-button">Sửa món</a>
					<a href="checkout.php" class="cart-button order-button">Đặt hàng</a>
				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
	function Tang(){
		var x = document.getElementById("quality").value;//lay gia tri cu trong text
		if(parseInt(x) > 0){
		document.getElementById("quality").value = parseInt(x) +1;// + gia tri lay dc len 1 roi gan kq vao o text
		}
	}
	function Giam(){
		var x = document.getElementById("quality").value;
		if(parseInt(x) > 1){
		document.getElementById("quality").value = parseInt(x) -1;
		}
	}
</script>
@endsection