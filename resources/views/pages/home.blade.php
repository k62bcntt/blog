@extends('layout.master')

@section('content')
<div class="home-page">
	<!-- Slider -->
<?php //require('pages/sections/slider.php'); ?>
<!-- End slider -->

<!-- Best selling -->
<section class="best-selling">
	<div class="container">
		<div class="section-title">
			<h2>Best selling</h2>
		</div>
		<div class="best-selling-content">
			<?php
				$array = [
					[
						'title' => 'Combo sáng năng lượng',
						'image' => URL::asset('/images/home-bestselling-1.jpg'),
						'price' => '49.000đ'
					],
					[
						'title' => 'Combo chiều tỉnh táo',
						'image' => URL::asset('/images/home-bestselling-2.jpg'),
						'price' => '49.000đ'
					],
					[
						'title' => 'Combo sáng năng lượng',
						'image' => URL::asset('/images/home-bestselling-1.jpg'),
						'price' => '49.000đ'
					],
					[
						'title' => 'Combo chiều tỉnh táo',
						'image' => URL::asset('/images/home-bestselling-2.jpg'),
						'price' => '49.000đ'
					],
					[
						'title' => 'Combo sáng năng lượng',
						'image' => URL::asset('/images/home-bestselling-1.jpg'),
						'price' => '49.000đ'
					],
					[
						'title' => 'Combo chiều tỉnh táo',
						'image' => URL::asset('/images/home-bestselling-2.jpg'),
						'price' => '49.000đ'
					],
					[
						'title' => 'Combo chiều tỉnh táo',
						'image' => URL::asset('/images/home-bestselling-1.jpg'),
						'price' => '49.000đ'
					],
				];
			?>
			<div class="product-slider">
				<div class="product-slider-title">
					<h3>Mango cheese tea</h3>
				</div>
				<div class="product-slider-list">
				<?php
					foreach ($array as $array_kq) { ?>

					<article class="item">
						<div class="image">
							<figure>
								<a href="pages/single-product.php">
								<img src="{{ URL::asset('/images/3x3.png') }}" style="background-image: url({{ $array_kq['image'] }})" alt="{{ $array_kq['title'] }}">
								</a>
							</figure>
							<div class="info">
								<div class="title">
									<h3>{{ $array_kq['title'] }}</h3>
								</div>
								<div class="price">
									{{ $array_kq['price'] }}
								</div>
							</div>
						</div>
					</article>
				<?php
					}
				?>
				</div>
			</div>
			<div class="product-slider">
				<div class="product-slider-title">
					<h3>Cheese tea</h3>
				</div>
				<div class="product-slider-list">
				@foreach ($array as $array_kq)
					<article class="item">
						<div class="image">
							<figure>
								<a href="pages/single-product.php">
									<img src="{{ URL::asset('3x3.png') }}" style="background-image: url(<?php echo $array_kq['image']; ?>)" alt="<?php echo $array_kq['title']; ?>">
								</a>
							</figure>
							<div class="info">
								<div class="title">
									<h3><?php echo $array_kq['title']; ?></h3>
								</div>
								<div class="price">
									<?php echo $array_kq['price']; ?>
								</div>
							</div>
						</div>
					</article>
				@endforeach
				</div>
			</div>
			<div class="read-more">
				<a href="">Xem thêm</a>
			</div>
		</div>
	</div>
</section>
<!-- end best selling -->

<!-- Sale tea -->
<section class="sale-tea">
	<div class="container">
		<div class="section-title">
			<h2>Sale tea</h2>
		</div>
		<div class="row sale-tea-content">
			<article class="item">
				<figure>
					<img src="{{ URL::asset('home-saletea-1.jpg') }}" alt="">
				</figure>
			</article>
			<article class="item">
				<figure>
					
					<img src="{{ URL::asset('home-saletea-2.jpg') }}" alt="">
				</figure>
			</article>
			<article class="item">
				<figure>
					
					<img src="{{ URL::asset('home-saletea-1.jpg') }}" alt="">
				</figure>
		</article>
		</div>
	</div>
</section>
<!-- End sale tea -->

<!-- Home banner -->
<section class="home-banner">
	<div class="home-banner-content">
		<img src="{{ URL::asset('home-banner.jpg') }}" alt="">
	</div>
</section>
<!-- End home banner -->

<!-- Home introduce -->
<section class="home-introduce">
	<div class="container">
		<div class="home-introduce-title">
			<h2>Leetee Viet Nam</h2>
		</div>
		<div class="home-introduce-content">
			At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum
		</div>
	</div>

</section>
<!-- End home introduce -->
</div>
@endsection

