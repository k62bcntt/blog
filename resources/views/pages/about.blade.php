@extends('layout.master')

@section('content')
<div class="about-page">
	<div class="banner"  style="background-image: url( {{ asset('assets/images/about/about-banner.png') }} );">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 banner-wrapper flex-box flex-items-end">
					<h1>Giới thiệu</h1>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="col-xs-12 nav-pills-wrapper">
				<ul class="nav nav-pills">
					<li class="active"><a data-toggle="pill" href="#vision">Sứ mệnh - Tầm nhìn</a></li>
					<li><a data-toggle="pill" href="#terms">Điều khoản sử dụng/bảo mật</a></li>
					<li><a data-toggle="pill" href="#news">Tin tức báo chí</a></li>
				</ul>
				
				<div class="tab-content">
					<div id="vision" class="tab-pane fade in active tab-content-wrapper">
						<h3>Sứ mệnh của chúng tôi</h3>
						<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>
						<div class="secondary-content">
							<img class="img-responsive" src="{{ asset('assets/images/about/quote-logo.png') }}">
							<h4>Sứ mệnh của chúng tôi</h4>
							<p>“Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem ape ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia ”</p>
						</div>
					</div>
					<div id="terms" class="tab-pane fade tab-content-wrapper">
						
					</div>
					<div id="news" class="tab-pane fade tab-content-wrapper">
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection