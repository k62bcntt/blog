@extends('layout.master')

@section('content')
<div class="google-map">
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.458251317354!2d105.81864461430787!3d21.014342593647655!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab7c5b82c90d%3A0x650e9ef8151f88fa!2zNDIgTmfDtSAxNzggVGjDoWkgSMOgLCBUcnVuZyBMaeG7h3QsIMSQ4buRbmcgxJBhLCBIw6AgTuG7mWksIFZpZXRuYW0!5e0!3m2!1sen!2s!4v1527212322355" width="100%" height="520" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
<section class="contact-us">
	<nav class="breadcrumbs">
		<div class="container">
			<span>
				<a href="<?php echo site_url('index.php'); ?>">Trang chủ</a> >
				<span class="breadcrumb_last">Liên hệ</span>
			</span>
		</div>
	</nav>
	<div class="container">
		<div class="contact-us-content">
			<div class="row justify-content-center">
				<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
					<div class="section-title">
						<h1>Liên hệ</h1>
					</div>
					<div class="form-contact">
						<form action="" class="contact-form">
							<div class="row">
								<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
									<div class="wrap-group">
										<input type="text" name="name" id="name" placeholder="Họ tên khách hàng">
									</div>
									<div class="wrap-group">
										<input type="email" name="email" id="email" placeholder="Email">
									</div>
									<div class="wrap-group">
										<input type="text" name="phone" id="phone" placeholder="Số điện thoại">
									</div>
									<div class="wrap-group">
										<input type="text" name="address" id="address" placeholder="Địa chỉ">
									</div>
								</div>
								<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
									<div class="wrap-group">
										<textarea name="note" id="note" placeholder="Nội dung"></textarea>
									</div>
								</div>
							</div>
							<div class="btn">
								<input type="submit" value="Gửi" class="submit">
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection