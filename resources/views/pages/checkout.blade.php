@extends('layout.master')

@section('content')
<section class="page-checkout">
    <div class="container">
        <div class="row">
		
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 page-checkout-content">

				<nav class="breadcrumbs">
					<div class="container">
						<span>
							<a href="<?php echo site_url('index.php'); ?>">Home</a> > 
							<a href="<?php echo site_url('index.php'); ?>">New</a> >
							<a href="<?php echo site_url('index.php'); ?>">My Bag</a> >
							<span class="breadcrumb_last">Check out</span>
						</span>
					</div>
				</nav>

				<div class="section-title">
					<h1></h1>
				</div>

				<div class="page-checkout-info">

					<div class="form-checkout-title">Thông tin khách hàng</div>

		            <form class="form-checkout-info" name="" nf-contact="" id="">
		                <div class="wrap-group">
		                    <input required="" class="form-control" placeholder="Họ tên khách hàng" name="" type="text" value="">
		                </div>
		                <div class="wrap-group">
		                    <input required="" class="form-control" placeholder="Email" name="" type="text" value="">
		                </div>
		                <div class="wrap-group">
		                    <input required="" class="form-control" placeholder="Số điện thoại" name="" type="text" value="">
		                </div>
		                <div class="wrap-group">
		                    <input required="" class="form-control" placeholder="Địa chỉ" name="" type="text" value="">
		                </div>
		                <div class="wrap-group">
		                    <input required="" class="form-control" placeholder="Quận" name="" type="text" value="">
		                </div>
						<div class="wrap-group">
							<select name="">
								<option value="">Huyện / Thành</option>
								<option value="">Hà Nội</option>
								<option value="">Hà Đông</option>
							</select>
						</div>
						<div class="wrap-group">
							<label for="country">Ghi chú</label>
							<textarea class="form-control" placeholder="" cols="10" rows="5" name=""></textarea>
						</div>
						<div class="payment-method">
							<div class="wrap-group">
								<div class="checkbox-group">
									<div class="checkbox-item">
									   <input type="radio" id="bon" name="contact-address-type" value="Home">
									   <label for="bon"><span>Thanh toán khi giao hàng</span></label>
									</div>
									<div class="checkbox-item">
									   <input type="radio" id="nam" name="contact-address-type" value="Destination">
									   <label for="nam"><span>Đến cửa hàng lấy hàng</span></label>
									</div>
								</div>
							</div>
						</div>
		            </form>

				</div>
			</div>

			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 page-checkout-order">
				<div class="order-review">

					<div class="order-review-content">
						<h3>Thông tin đơn hàng</h3>
						<table class="shop_table woocommerce-checkout-review-order-table table">
							<tbody>
								<tr class="cart_item">
									<!-- <th class="product-image">
										<img src="<?php echo asset_image('cart-image.png'); ?>" alt="">
									</th> -->
									<th class="product-name">
										<img src="<?php echo asset_image('cart-image.png'); ?>" alt="">
										<!-- <span> -->
										Fresh juice tea <br>
										M(L70% S30%)
										<!-- </span> -->
									</th>
									<th class="product-quality">x2</th>
									<th class="product-total right">69.000 VND</th>
								</tr>
							</tbody>
						</table>
						<div class="order-total">
							<span>Tổng</span>
							<span>138.000 VND</span>
						</div>
					</div>

					<div class="wc-proceed-to-checkout">
						<a href="" class="cart-button">Đặt hàng</a>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>
@endsection