<div class="menu col-sm-2 p-r-0">
    <div class="sidebar-nav">
        <div class="navbar navbar-default" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <span class="visible-xs navbar-brand">Danh mục</span>
            </div>
            <div class="navbar-collapse collapse sidebar-navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="{{route('customer.profile')}}">HỒ SƠ KHÁCH HÀNG</a></li>
                    <li><a href="#">ĐANG YÊU CẦU</a></li>
                    <li><a href="#">ĐANG THỰC HIỆN</a></li>
                    <li><a href="#">HOÀN THÀNH</a></li>
                    <li><a href="#">KHÁC</a></li>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </div>
</div>