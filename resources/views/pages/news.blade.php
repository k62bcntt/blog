@extends('layout.master')

@section('title')
Uplaw | Tin tức
@endsection

@section('og_title')
Uplaw | Tin tức
@endsection

@section('content')
<div class="legalblog-page">
	<div class="banner"  style="background-image: url( {{ asset('assets/images/legalblog/news-banner.png') }} );">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 banner-wrapper flex-box flex-items-end">
					<h1>Legal Blog</h1>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
			@include('partials.legalblog-right-column')
			
			<div class="col-xs-12 col-sm-9 col-sm-pull-3 legalblog-list">
				@include('partials.legalblog-topnav')
				
				<div class="legalblog-list-content">
					@if(count($news) > 0)
						@foreach($news as $new)
						<div class="legalblog-item">
							<h3><a href="{!! route('new.show', ['slug' => $new->slug]) !!}">{{$new->title}}</a></h3>
							<p class="date">Ngày đăng: {{$new->updated_at->format('d/m/Y')}}</p>
							<p class="question">{!! str_limit($new->content, 500) !!}</p>
						</div>
						@endforeach
					@endif
					<nav aria-label="Page navigation">
						{{-- <ul class="pagination">
							<li class="disabled">
								<span>
									<span aria-hidden="true">&laquo;</span>
								</span>
							</li>
							<li class="active"><a href="#">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#">5</a></li>
							<li>
								<a href="#" aria-label="Next">
									<span aria-hidden="true">&raquo;</span>
								</a>
							</li>
						</ul> --}}
						{{ $news->links() }}
					</nav>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection