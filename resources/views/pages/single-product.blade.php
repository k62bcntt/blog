@extends('layout.master')

@section('content')
<!-- Slider -->
<?php require('sections/slider.php'); ?>
<!-- End slider -->

<nav class="breadcrumbs">
	<div class="container">
		<span>
			<a href="<?php echo site_url('index.php'); ?>">Home</a> >
			<span class="breadcrumb_last">New</span>
		</span>
	</div>
</nav>
<section class="single-product">
	<div class="container">
		<div class="section-title">
			<h1>Fresh juice tea</h1>
		</div>
		<div class="single-product-content">
			<div class="row">
				<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 product-image">
					<div class="full-image">
						<img src="<?php echo asset_image('single-product-image.png'); ?>" alt="">
					</div>
				</div>
				<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 product-info">
					<div class="pro-desc">
						Được làm từ thanh trà và hoa quả tươi sẽ giúp bạn tỉnh táo và #Detox cơ thể cực hiệu quả . Giải pháp cực tốt cho những ngày hè sắp tới .
					</div>
					<div class="pro-price">
						<span><span class="red">38,000 </span>VND</span>
					</div>
					<div class="pro-topping">
						<span class="red">+</span> 3000 Thạch trà xanh <span class="red">+</span> 3000 Kem cheese
					</div>
					<div class="pro-choice pro-size">
						<span class="prochoice-title">Size</span>
						<ul>
							<li class="active">M</li>
							<li>L</li>
						</ul>
					</div>
					<div class="clear"></div>
					<div class="pro-choice pro-ice-sugar">
						<div class="pro-ice">
							<span class="prochoice-title">Ice - %</span>
							<ul>
								<li class="ice-active">
									<img src="<?php echo asset_image('single-product-ice-pink.png'); ?>" alt="">
									<span>70</span>
								</li>
								<li>
									<img src="<?php echo asset_image('ice-50.png'); ?>" alt="">
									<span>50</span>
								</li>
								<li>
									<img src="<?php echo asset_image('ice-30pink.png'); ?>" alt="">
									<span>30</span>
								</li>
							</ul>
						</div>
						<div class="clear"></div>
						<div class="pro-sugar">
							<span class="prochoice-title">Sugar - %</span>
							<ul>
								<li class="ice-active">
									<img src="<?php echo asset_image('single-product-ice-pink.png'); ?>" alt="">
									<span>70</span>
								</li>
								<li>
									<img src="<?php echo asset_image('ice-50.png'); ?>" alt="">
									<span>50</span>
								</li>
								<li>
									<img src="<?php echo asset_image('ice-30pink.png'); ?>" alt="">
									<span>30</span>
								</li>
							</ul>
						</div>
						<div class="clear"></div>
					</div>
					<div class="pro-choice pro-add">
						<span class="prochoice-title">Món thêm</span>
						<select name="tea" id="">
							<option value="">Chọn thêm món</option>
							<option value="mangocheese">Trà nhài mango cheese</option>
							<option value="olong">trà Ô Long Leetee</option>
							<option value="socolalatte">Socola Latte</option>
						</select>
					</div>
					<div class="clear"></div>
					<div class="read-more">
						<a href="cart.php">Chọn</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection