@extends('layout.master')

@section('content')
<section class="search-page">
	<div class="search-page-content">
		<div class="container">
			<div class="search-title">
				<span>23 Kết quả tìm kiếm cho “Trà”</span>
			</div>
			<div class="search-list">
				<?php
					$array = [
						[
							'title' => 'Combo sáng năng lượng',
							'image' => asset_image('home-bestselling-1.jpg'),
							'price' => '49.000đ'
						],
						[
							'title' => 'Combo chiều tỉnh táo',
							'image' => asset_image('home-bestselling-2.jpg'),
							'price' => '49.000đ'
						],
						[
							'title' => 'Combo sáng năng lượng',
							'image' => asset_image('home-bestselling-1.jpg'),
							'price' => '49.000đ'
						],
						[
							'title' => 'Combo chiều tỉnh táo',
							'image' => asset_image('home-bestselling-2.jpg'),
							'price' => '49.000đ'
						],
						[
							'title' => 'Combo sáng năng lượng',
							'image' => asset_image('home-bestselling-1.jpg'),
							'price' => '49.000đ'
						],
						[
							'title' => 'Combo chiều tỉnh táo',
							'image' => asset_image('home-bestselling-2.jpg'),
							'price' => '49.000đ'
						],
						[
							'title' => 'Combo chiều tỉnh táo',
							'image' => asset_image('home-bestselling-1.jpg'),
							'price' => '49.000đ'
						],
						[
							'title' => 'Combo sáng năng lượng',
							'image' => asset_image('home-bestselling-1.jpg'),
							'price' => '49.000đ'
						],
						[
							'title' => 'Combo chiều tỉnh táo',
							'image' => asset_image('home-bestselling-2.jpg'),
							'price' => '49.000đ'
						],
						[
							'title' => 'Combo sáng năng lượng',
							'image' => asset_image('home-bestselling-1.jpg'),
							'price' => '49.000đ'
						],
						[
							'title' => 'Combo chiều tỉnh táo',
							'image' => asset_image('home-bestselling-2.jpg'),
							'price' => '49.000đ'
						],
					];
				?>
				<div class="row">
					<?php
						foreach ($array as $array_kq) { ?>

						<article class="col-md-3 item">
							<div class="image">
								<figure>
									<a href="pages/single-product.php">
										<img src="<?php echo asset_image('3x3.png'); ?>" style="background-image: url(<?php echo $array_kq['image']; ?>)" alt="<?php echo $array_kq['title']; ?>">
									</a>
								</figure>
								<div class="info">
									<div class="title">
										<h3><?php echo $array_kq['title']; ?></h3>
									</div>
									<div class="price">
										<?php echo $array_kq['price']; ?>
									</div>
								</div>
							</div>
						</article>
								
					<?php
						}
					?>
				</div>
			</div>
			<?php require('sections/pagination.php') ?>
		</div>
	</div>
</section>
@endsection