@extends('layout.master')

@section('head')
	@parent
	<base href="/">
@endsection

@section('content')
	<app-root></app-root>
@endsection

@section('foot')
	@parent

	@if(env('APP_ENV') == 'production') 
        @if(file_exists( public_path() . '/webapp/app/inline.bundle.js' ))
        <script src="{{ asset('webapp/app/inline.bundle.js') }}"></script>
        @endif

        @if(file_exists( public_path() . '/webapp/app/polyfills.bundle.js' ))
        <script src="{{ asset('webapp/app/polyfills.bundle.js') }}"></script>
        @endif

        @if(file_exists( public_path() . '/webapp/app/scripts.bundle.js' ))
        <script src="{{ asset('webapp/app/scripts.bundle.js') }}"></script>
        @endif

        @if(file_exists( public_path() . '/webapp/app/styles.bundle.js' ))
        <script src="{{ asset('webapp/app/styles.bundle.js') }}"></script>
        @endif

        @if(file_exists( public_path() . '/webapp/app/vendor.bundle.js' ))
        <script src="{{ asset('webapp/app/vendor.bundle.js') }}"></script>
        @endif

        @if(file_exists( public_path() . '/webapp/app/main.bundle.js' ))
        <script src="{{ asset('webapp/app/main.bundle.js') }}"></script>
        @endif
    @else
            <script src="{{ asset('webapp/app/inline.bundle.js') }}"></script>

            <script src="{{ asset('webapp/app/polyfills.bundle.js') }}"></script>

            <script src="{{ asset('webapp/app/scripts.bundle.js') }}"></script>

            <script src="{{ asset('webapp/app/styles.bundle.js') }}"></script>

            <script src="{{ asset('webapp/app/vendor.bundle.js') }}"></script>

            <script src="{{ asset('webapp/app/main.bundle.js') }}"></script>
    @endif
@endsection