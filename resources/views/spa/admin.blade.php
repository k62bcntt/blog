<html>

<head>
    <base href="/admin">
    <title>Uplaw - Admin</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=0.5, maximum-scale=1.0" />
    <meta NAME="ROBOTS" CONTENT="NOINDEX, FOLLOW" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body class="nav-md">
    <app-root></app-root>
</body>
<footer>
    <script src="{{ asset('webapp/admin/inline.bundle.js') }}"></script>
    <script src="{{ asset('webapp/admin/polyfills.bundle.js') }}"></script>
    <script src="{{ asset('webapp/admin/scripts.bundle.js') }}"></script>
    <script src="{{ asset('webapp/admin/styles.bundle.js') }}"></script>
    <script src="{{ asset('webapp/admin/vendor.bundle.js') }}"></script>
    <script src="{{ asset('webapp/admin/main.bundle.js') }}"></script>
</footer>

</html>