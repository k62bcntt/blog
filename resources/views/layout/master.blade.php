<!DOCTYPE html>
<html lang="vi">
    
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="{{ asset('assets/images/logo.png') }}">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title', 'LeeTee')</title>
        <meta property="og:image" content="@yield('og_image', asset("/assets/images/logo.png"))"/>
        <meta property="og:url" content="{!! url()->current() !!}">
        <meta property="og:type" content="website">
        <meta property="og:title" content="@yield('og_title', 'leetee.com')">
        <meta property="og:description" content="@yield('og_description', 'leetee.com')">
        
        <!-- Styles -->
        @section('head')
        <link href="{{ asset('assets/dist/app.css') }}" rel="stylesheet">
        @show
        
    </head>
    
    <body>
        @include('partials.header')
        @if (session('flash_message') || (isset($errors) && count($errors) > 0))
        <div class="vc-alert-wrap">
            <div class="container">
                @if (session('flash_message') )
                <div class="alert alert-{{ session('flash_level') }}">
                    {{ session('flash_message') }}
                </div>
                @endif
                
                @if (isset($errors) && count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
            </div>
        </div>
        @endif
        @yield('content')
        
        @include('partials.footer')
        
        <!-- Scripts -->
        @section('foot')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.4/socket.io.js"></script>
        <script src="{{ asset('assets/dist/app.js') }}"></script>
        @show
    </body>
    
</html>