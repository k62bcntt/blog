<footer class="footer">
	<div class="container">
		<div class="footer-top">
			<div class="footer-logo">
			<img src="{{ asset('assets/images/logo-footer.png') }}" alt="">
			</div>
		</div>
		<div class="footer-midle">
			<div class="row">
				<div class="col-xl-2 col-lg-2 col-md-6 col-sm-6 col-12 footer-midle-content">
					<div class="footer-title">
						<h3>Company</h3>
					</div>
					<div class="footer-content">
						<ul>
							<li>
								<a href="">Careers</a>
							</li>
							<li>
								<a href="">About</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-xl-2 col-lg-2 col-md-6 col-sm-6 col-12 footer-midle-content">
					<div class="footer-title">
						<h3>Support</h3>
					</div>
					<div class="footer-content">
						<ul>
							<li>
								<a href="">Contact Us</a>
							</li>
							<li>
								<a href="">FAQs</a>
							</li>
							<li>
								<a href="">Privacy Policy</a>
							</li>
							<li>
								<a href="">What is verified account</a>
							</li>
							<li>
								<a href="">What is verified account</a>
							</li>
							<li>
								<a href="">What is verified account</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-xl-2 col-lg-2 col-md-6 col-sm-6 col-12 footer-midle-content">
					<div class="footer-title">
						<h3>Locations</h3>
					</div>
					<div class="footer-content">
						<ul>
							<li>
								<a href="">Ho Chi Minh city</a>
							</li>
							<li>
								<a href="">District 1</a>
							</li>
							<li>
								<a href="">District 2</a>
							</li>
							<li>
								<a href="">District 3</a>
							</li>
							<li>
								<a href="">District 4</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-xl-2 col-lg-2 col-md-6 col-sm-6 col-12 footer-midle-content">
					<div class="footer-title">
						<h3>Project</h3>
					</div>
					<div class="footer-content">
						<ul>
							<li>
								<a href="">Masteri Thao Dien
								</a>
							</li>
							<li>
								<a href="">Vinhomes Central Park</a>
							</li>
							<li>
								<a href="">Scenic Valley</a>
							</li>
							<li>
								<a href="">Saigon Pearl</a>
							</li>
							<li>
								<a href="">Tropic Garden</a>
							</li>
							<li>
								<a href="">Tropic Garden</a>
							</li>
							<li>
								<a href="">Tropic Garden</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12 footer-midle-content footer-connected">
					<div class="footer-title">
						<h3>Stay Connected</h3>
					</div>
					<div class="footer-connected-content">
						<p>Join over 3000 people who receive rental advice and best proper deals</p>
						<form action="" class="email-subcribe">
							<input type="email" name="email" id="email" placeholder="Email address ...">
							<input type="submit" value="Submit">
						</form>
						<div class="footer-social">
							<ul>
								<li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
								<li><a href=""><i class="fa fa-twitter"></i></a></li>
								<li><a href=""><i class="fa fa-youtube"></i></a></li>
								<li><a href=""><i class="fa fa-google-plus"></i></a></li>
								<li><a href=""><i class="fa fa-instagram"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-bottom">
		<div class="container">
			<div class="footer-bottom-content">
				Copyright <span> Leeteevietnam</span>.  All Rights Reserved.
			</div>
		</div>
	</div>
</footer>