<header>
    <div class="container">
        <div class="header-content">
            <div class="menu">
                <nav class="menu-primary">
                    <div class="main-menu">
                        <div>
                            <ul class="menu-primary">
                                <li class="menu-item">
                                    <a href="">Sản phẩm</a>
                                </li>
                                <li class="menu-item">
                                    <a href="">Nhượng quyền</a>
                                </li>
                                <li class="menu-item">
                                    <a href="">Liên hệ</a>
                                </li>
                                <li class="menu-item">
                                    <a href="">Giỏ hàng</a>
                                </li>
                                @if (Auth::user())
                                <li class="menu-item auth-dropdown">
                                    <a class="dropdown-toggle" href="javascript:;">
                                            <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                                        </a>
                                    <ul class="dropdown-menu dropdown-user">
                                        <li>
                                            @if (Auth::user()->isRole('customer'))
                                                <a href="/customer/profile"><i class="fa fa-user fa-fw"></i> Hồ sơ Khách hàng</a>
                                            @endif
                                        </li>
                                        @if (Auth::user()->isRole('customer'))
                                        <li class="divider"></li>
                                        @endif
                                        <li>
                                            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                            document.getElementById('logout-form').submit();"><i class="fa fa-sign-out fa-fw"></i> Đăng xuất
                                                </a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                    <div class="mobile-menu"></div>
                </nav>
                <div class="search-box">
                    <form action="">
                        <input type="text" placeholder="" id="search-box" name="s" value="">
                        <div class="search-icon">
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </div>
                    </form>
                </div>
            </div>
            <div class="logo">
                <a href="{{ URL::to('/') }}">
                    <img src="{{ asset('assets/images/logo.png') }}" alt="Leetee">
                </a>
            </div>
        </div>
    </div>
</header>
