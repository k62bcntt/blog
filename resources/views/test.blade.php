<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <link rel="stylesheet" href="">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <style type="text/css" media="screen">
    .popup {
      width:100%;
      height:100%;
      display:none;
      position:fixed;
      top:0px;
      left:0px;
      background:rgba(0,0,0,0.75);
    }
    /* Inner */
    .popup-inner {
      max-width:700px;
      width:90%;
      padding:40px;
      position:absolute;
      top:50%;
      left:50%;
      -webkit-transform:translate(-50%, -50%);
      transform:translate(-50%, -50%);
      box-shadow:0px 2px 6px rgba(0,0,0,1);
      border-radius:3px;
      background:#fff;
    }
    
    </style>
  </head>
  <body>
    <a class="btn" data-popup-open="popup-1" href="#">Open Popup #1</a>
    <div class="popup" data-popup="popup-1">
      <div class="popup-inner">
        <h2>Wow! This is Awesome! (Popup #1)</h2>
        <p>Donec in volutpat nisi. In quam lectus, aliquet rhoncus cursus a, congue et arcu. Vestibulum tincidunt neque id nisi pulvinar aliquam. Nulla luctus luctus ipsum at ultricies. Nullam nec velit dui. Nullam sem eros, pulvinar sed pellentesque ac, feugiat et turpis. Donec gravida ipsum cursus massa malesuada tincidunt. Nullam finibus nunc mauris, quis semper neque ultrices in. Ut ac risus eget eros imperdiet posuere nec eu lectus.</p>
        
        <a class="popup-close" data-popup-close="popup-1" href="#">x</a>
      </div>
    </div>
    <script type="text/javascript">
      $(function() {
        //----- OPEN
        $('[data-popup-open]').on('click', function(e)  {
          var targeted_popup_class = jQuery(this).attr('data-popup-open');
          $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
          e.preventDefault();
          });
        //----- CLOSE
        $('[data-popup-close]').on('click', function(e)  {
          var targeted_popup_class = jQuery(this).attr('data-popup-close');
          $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
          e.preventDefault();
        });
      
      });
    </script>
  </body>
</html>