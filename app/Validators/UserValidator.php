<?php

namespace App\Validators;

use App\Validators\AbstractValidator;

class UserValidator extends AbstractValidator
{

    protected $rules = [
        'ADMIN_CREATE_USER'             => [
            'email'        => ['required', 'email'],
            'avatar'       => ['required'],
            'password'     => ['required', 'min:6', 'max:30'],
            'first_name'   => ['required', 'regex:/[a-z0-9\s]*/i', 'max:20'],
            'last_name'    => ['required', 'regex:/[a-z0-9\s]*/i', 'max:20'],
            'phone_number' => ['required', 'regex:/^\d+$/', 'max:15'],
            'birth'        => ['required'],
            'role'         => ['required', 'regex:/[user|manager]/'],
        ],
        'ADMIN_UPDATE_USER'             => [
            'email'        => ['required', 'email'],
            'first_name'   => ['required', 'regex:/[a-z0-9\s]*/i', 'max:20'],
            'last_name'    => ['required', 'regex:/[a-z0-9\s]*/i', 'max:20'],
            'phone_number' => ['required', 'regex:/^\d+$/', 'max:15'],
            'birth'        => ['required'],
        ],
        'RULE_CREATE'                   => [
            'email'        => ['required', 'email'],
            'avatar'       => [],
            'password'     => ['required', 'min:6', 'max:30'],
            'first_name'   => ['required', 'regex:/[a-z0-9\s]*/i', 'max:20'],
            'last_name'    => ['required', 'regex:/[a-z0-9\s]*/i', 'max:20'],
            'phone_number' => ['required', 'regex:/^\d+$/', 'max:15'],
            'birth'        => [],
            'role'         => ['required', 'regex:/[user|manager]/'],
        ],
        'RULE_UPDATE'                   => [
            'email'        => ['required', 'email'],
            'first_name'   => ['required', 'regex:/[a-z0-9\s]*/i', 'max:20'],
            'last_name'    => ['required', 'regex:/[a-z0-9\s]*/i', 'max:20'],
            'phone_number' => ['required', 'regex:/^\d+$/', 'max:15'],
            'birth'        => ['required'],
        ],
        'CHANGE_PASSWORD'               => [
            'email'        => ['required'],
            'old_password' => ['required'],
            'new_password' => ['required'],
        ],
        'UPDATE_PASSWORD'               => [
            'email'    => ['required'],
            'password' => ['required'],
        ],
        'IMAGES'                        => [
            'url' => ['required', 'url'],
        ],
        'VERIFY_EMAIL'                  => [
            'token' => ['required'],
        ],
        'UPDATE_ROLE'                   => [
            'role' => ['required', 'regex:/[user|manager]/'],
        ],
        'DELETE_FILE'                   => [
            'path' => ['required'],
        ],
        'UPDATE_PRACTICING_CERTIFICATE' => [
            'practicing_certificate' => ['required'],
        ],
        'PROFILE_UPDATE'                => [
            'email'      => ['required', 'email'],
            'first_name' => ['required', 'regex:/[a-z0-9\s]*/i', 'max:20'],
            'last_name'  => ['required', 'regex:/[a-z0-9\s]*/i', 'max:20'],
            'birth'      => ['required'],
        ],
        'CHANGE_STATUS_ALL_ITEMS'       => [
            'item_ids' => ['required'],
            'status'   => ['required'],
        ],
        'CHANGE_STATUS_ITEM'            => [
            'status' => ['required'],
        ],
    ];
}
