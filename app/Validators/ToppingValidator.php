<?php

namespace App\Validators;

use App\Validators\AbstractValidator;

class ToppingValidator extends AbstractValidator
{

    protected $rules = [
        AbstractValidator::RULE_CREATE             => [
            'name'  => ['required'],
            'price' => ['required'],
        ],
        AbstractValidator::RULE_UPDATE             => [
            'name'  => ['required'],
            'price' => ['required'],
        ],
        AbstractValidator::CHANGE_STATUS_ALL_ITEMS => [
            'item_ids' => ['required'],
            'status'   => ['required'],
        ],
        AbstractValidator::CHANGE_STATUS_ITEM      => [
            'status' => ['required'],
        ],
    ];
}
