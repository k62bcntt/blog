<?php

namespace App\Validators;

use App\Validators\AbstractValidator;

class StoreValidator extends AbstractValidator
{

    protected $rules = [
        AbstractValidator::RULE_CREATE             => [
            'name'         => ['required'],
            'address'      => ['required'],
            'phone_number' => ['required'],
            'user_id'      => ['required'],
        ],
        AbstractValidator::RULE_UPDATE             => [
            'name'         => ['required'],
            'address'      => ['required'],
            'phone_number' => ['required'],
            'user_id'      => ['required'],
        ],
        AbstractValidator::CHANGE_STATUS_ALL_ITEMS => [
            'item_ids' => ['required'],
            'status'   => ['required'],
        ],
        AbstractValidator::CHANGE_STATUS_ITEM      => [
            'status' => ['required'],
        ],
    ];
}
