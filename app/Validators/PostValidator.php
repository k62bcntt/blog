<?php

namespace App\Validators;

use App\Validators\AbstractValidator;

class PostValidator extends AbstractValidator
{

    protected $rules = [
        AbstractValidator::RULE_CREATE             => [
            'title'       => ['required'],
            'category_id' => ['required'],
            'user_id'     => ['required'],
            'content'     => ['required'],
        ],
        AbstractValidator::RULE_UPDATE             => [
            'title'       => ['required'],
            'category_id' => ['required'],
            'user_id'     => ['required'],
            'content'     => ['required'],
        ],
        AbstractValidator::CHANGE_STATUS_ALL_ITEMS => [
            'item_ids' => ['required'],
            'status'   => ['required'],
        ],
        AbstractValidator::CHANGE_STATUS_ITEM      => [
            'status' => ['required'],
        ],
    ];
}
