<?php

namespace App\Validators;

use App\Validators\AbstractValidator;

class FileValidator extends AbstractValidator
{

    protected $rules = [
        'RULE_CREATE' => [
            'file'        => ['required', 'mimes:jpeg,png,gif,pdf,doc,docx'],
            'upload_path' => ['required', 'regex:/[a-z]*/'],
        ],
    ];
}
