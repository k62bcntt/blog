<?php

namespace App\Validators;

use App\Validators\AbstractValidator;

class ProductValidator extends AbstractValidator
{

    protected $rules = [
        AbstractValidator::RULE_CREATE             => [
            'name'        => ['required'],
            'image'       => ['required'],
            'description' => ['required'],
            'price'       => ['required'],
            'sale_price'  => ['required'],
        ],
        AbstractValidator::RULE_UPDATE             => [
            'name'        => ['required'],
            'description' => ['required'],
            'price'       => ['required'],
            'sale_price'  => ['required'],
        ],
        AbstractValidator::CHANGE_STATUS_ALL_ITEMS => [
            'item_ids' => ['required'],
            'status'   => ['required'],
        ],
        AbstractValidator::CHANGE_STATUS_ITEM      => [
            'status' => ['required'],
        ],
    ];
}
