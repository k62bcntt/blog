<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Topping extends Model
{
    const STATUS_ACTIVE  = 1;
    const STATUS_PENDING = 0;

    protected $table = 'toppings';

    protected $fillable = [
        'name',
        'price',
    ];
}
