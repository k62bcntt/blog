<?php

namespace App\Entities;

use App\Entities\Post;
use App\Entities\Store;
use App\Notifications\MailResetPasswordToken;
use Illuminate\Auth\Passwords\CanResetPassword as CanResetPasswordTrait;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use NF\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;
use NF\Roles\Models\Role;
use NF\Roles\Traits\HasRoleAndPermission;
use Prettus\Repository\Traits\TransformableTrait;
use Tymon\JWTAuth\Facades\JWTAuth;

class User extends Authenticatable implements HasRoleAndPermissionContract, CanResetPassword
{
    use Notifiable;
    use TransformableTrait;
    use HasRoleAndPermission;
    use CanResetPasswordTrait;

    const STATUS_ACTIVE  = 1;
    const STATUS_PENDING = 0;

    const EMAIL_VERIFIED     = 1;
    const EMAIL_NOT_VERIFIED = 0;

    protected $fillable = [
        'email',
        'first_name',
        'last_name',
        'avatar',
        'birth',
        'phone_number',
    ];

    /**
     * Use to custom view in reset password mail
     * @param  [type] $token [description]
     * @return [type]        [description]
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MailResetPasswordToken($token));
    }

    public function getToken()
    {
        return JWTAuth::fromUser($this);
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * Generate email verify token
     *
     * @return string
     */
    public function getEmailVerifyToken()
    {
        return Hash::make($this->email);
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function stores()
    {
        return $this->hasMany(Store::class);
    }
}
