<?php

namespace App\Entities;

use App\Entities\Category;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    const STATUS_ACTIVE  = 1;
    const STATUS_PENDING = 0;

    const SIZE_M = 1;
    const SIZE_L = 2;

    protected $table = 'products';

    protected $fillable = [
        'name',
        'slug',
        'image',
        'description',
        'price',
        'sale_price',
    ];

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }
}
