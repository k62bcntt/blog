<?php

namespace App\Entities;

use App\Entities\User;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    const STATUS_ACTIVE  = 1;
    const STATUS_PENDING = 0;

    protected $fillable = [
        'title',
        'slug',
        'image',
        'description',
        'content',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
