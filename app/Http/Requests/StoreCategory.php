<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCategory extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->id;
        return [
            'cate_name' => 'required|unique:categories,cate_name,'.$id
        ];
    }

    public function messages()
    {
        return [
            'cate_name.required' => 'Category name is required',
            'cate_name.unique'  => 'This Category name has already been used! Please insert another name!',
        ];
    }
}
