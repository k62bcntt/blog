<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OnepayCreatRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    public function fillData()
    {
        return [
            'transaction_no' => $this->get('vpc_TransactionNo'),
            'secure_hash'    => $this->get('vpc_SecureHash'),
            'merch_txn_ref'  => $this->get('vpc_MerchTxnRef'),
            'merchant'       => $this->get('vpc_Merchant'),
            'response_code'  => $this->get('vpc_TxnResponseCode'),
            'requirement_id' => $this->get('vpc_OrderInfo'),
            'amount'         => $this->get('vpc_Amount'),
            'local'          => $this->get('vpc_Locale'),
        ];
    }
}
