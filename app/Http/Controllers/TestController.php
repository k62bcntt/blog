<?php

namespace App\Http\Controllers;

use App\Entities\User;
use App\Http\Controllers\Api\ApiController;
use App\Repositories\RequirementRepository;
use App\Transformers\UserTransformer;

class TestController extends ApiController
{
    private $repository;

    public function __construct(RequirementRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        // $lawyer_online_ids = [1, 2, 3];

        // $query = new User;
        // $query = $query->orderBy('id', 'desc');

        // $query->orWhereIn('id', $lawyer_online_ids);

        // $users = $query->paginate();

        // return $this->response->paginator($users, new UserTransformer);
    }
}
