<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function logout(Request $request)
    {
        setcookie($this->jwt_token_key, null, -1, '/');
        Auth::logout();

        return redirect('/login');
    }
}
