<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class ResetPasswordController extends ApiController
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
     */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function sendResetResponse($response)
    {
        $token = Auth::user()->getToken();
        return $this->response->array(['token' => $token]);
    }

    public function sendResetFailedResponse($response)
    {
        throw new UnauthorizedHttpException('', "Token doesn't match or expired", null, 1003);
    }

    public function checkResetPasswordToken(Request $request)
    {
        $count = DB::table('password_resets')->where('email', $request->get('email'))->where('token', $request->get('token'))->count();
        if ($count === 0) {
            throw new UnauthorizedHttpException('', "Token doesn't match or expired", null, 1003);
        }
        return $this->success();
    }
}
