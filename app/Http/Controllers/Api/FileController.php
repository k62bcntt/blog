<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\NotFoundException;
use App\Http\Controllers\Api\ApiController;
use App\Validators\FileValidator;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class FileController extends ApiController
{
    private $validator;

    public function __construct(FileValidator $validator)
    {
        $this->validator = $validator;
    }

    public function upload(Request $request)
    {
        $data = $request->all();
        $this->validator->isValid($data, 'RULE_CREATE');

        if (!$request->hasFile('file')) {
            throw new NotFoundException("File");
        } else {

            $file           = $request->file('file');
            $origin_name    = $file->getClientOriginalName();
            $file_extension = $file->getClientOriginalExtension();

            $upload_path = $request->get('upload_path');

            if (!is_null($file_extension)) {
                $file_name = snake_case(str_replace('.' . $file_extension, '', $origin_name));
                $path      = $this->upload_directory . "/" . $upload_path . "/" . time() . "_{$file_name}.{$file_extension}";
            } else {
                $path = $this->upload_directory . "/" . $upload_path . "/" . time() . "_{$file_name}";
            }
            $path = strtolower($path);

            $success = Storage::disk('local')->put($path, File::get($file));

            if ($success) {
                return $this->response->array(['success' => $success, 'path' => url($path)]);
            } else {
                return $this->response->error('can\'t upload file', 1009);
            }
        }
    }

    public function delete($path)
    {
        try {
            if (Storage::disk('local')->has($path)) {
                if (is_dir($path)) {
                    Storage::disk('local')->deleteDirectory($path);
                } else {
                    Storage::disk('local')->delete($path);
                }
            }
            return true;
        } catch (Exception $e) {
            throw new Exception($e, 1);
        }
    }
}
