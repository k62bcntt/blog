<?php

namespace App\Http\Controllers\Api\Frontend;

use App\Entities\User;
use App\Http\Controllers\Api\ApiController;
use App\Repositories\UserRepository;
use App\Transformers\UserTransformer;
use App\Validators\UserValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserController extends ApiController
{
    private $repository;
    private $validator;

    public function __construct(UserRepository $repository, UserValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->middleware('jwt.auth', ['except' => ['store']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = new User;

        if ($request->has('role')) {
            $query = $query->whereHas('roles', function ($q) use ($request) {
                if ($request->get('role') != '') {
                    $q->where('slug', $request->get('role'));
                }
            });
        }
        if ($request->has('roles')) {
            $query = $query->whereHas('roles', function ($q) use ($request) {
                $q->whereIn('slug', explode(',', $request->get('roles')));
            });
        }

        $constraints = (array) json_decode($request->get('constraints'));
        if (count($constraints)) {
            $query = $query->where($constraints);
        }

        if ($request->has('search')) {
            $search       = trim($request->get('search'));
            $search_array = explode(' ', $search);

            if (count($search_array) == 1) {
                $first_name = $search_array[0];
                $last_name  = '';
            }

            if (count($search_array) > 1) {
                $first_name = $search_array[0];

                $remain_array = $search_array;
                unset($remain_array[0]);

                $last_name = implode(" ", $remain_array);
            }

            if (isset($first_name) && $first_name != '') {
                $query = $query->where('first_name', 'like', "%{$first_name}%");

                if (isset($last_name) && $last_name != '') {
                    $query = $query->orWhere('last_name', 'like', "%{$last_name}%");
                } else {
                    $query = $query->orWhere('last_name', 'like', "%{$first_name}%");
                }
            }
        }

        if ($request->has('order_by')) {
            $orderBy = (array) json_decode($request->get('order_by'));
            if (count($orderBy) > 0) {
                foreach ($orderBy as $key => $value) {
                    $query = $query->orderBy($key, $value);
                }
            }
        } else {
            $query = $query->orderBy('id', 'desc');
        }

        $per_page = $request->has('per_page') ? (int) $request->get('per_page') : 15;
        $users    = $query->paginate($per_page);

        if ($request->has('includes')) {
            $transformer = new UserTransformer(explode(',', $request->get('includes')));
        } else {
            $transformer = new UserTransformer;
        }
        return $this->response->paginator($users, $transformer);
    }

    public function getAll(Request $request)
    {
        $query = new User;

        if ($request->has('role')) {
            $query = $query->whereHas('roles', function ($q) use ($request) {
                $q->where('slug', $request->get('role'));
            });
        }
        if ($request->has('roles')) {
            $query = $query->whereHas('roles', function ($q) use ($request) {
                $q->whereIn('slug', explode(',', $request->get('roles')));
            });
        }

        $constraints = (array) json_decode($request->get('constraints'));
        if (count($constraints)) {
            $query = $query->where($constraints);
        }

        if ($request->has('search')) {
            $search = $request->get('search');
            $query  = $query->where(function ($q) use ($request, $search) {
                $q->where('first_name', 'like', "%{$search}%");
                $q->orWhere('last_name', 'like', "%{$search}%");
            });
        }

        if ($request->has('order_by')) {
            $orderBy = (array) json_decode($request->get('order_by'));
            if (count($orderBy) > 0) {
                foreach ($orderBy as $key => $value) {
                    $query = $query->orderBy($key, $value);
                }
            }
        }
        $users = $query->get();

        if ($request->has('includes')) {
            $transformer = new UserTransformer(explode(',', $request->get('includes')));
        } else {
            $transformer = new UserTransformer;
        }
        return $this->collection($users, $transformer);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validator->isValid($request, 'RULE_CREATE');
        if (!$this->repository->skipCache()->findByField('email', $request->get('email'))->isEmpty()) {
            throw new ConflictHttpException('Email already exist', null, 1001);
        }
        $data           = $request->all();
        $user           = $this->repository->create($data);
        $user->password = Hash::make($data['password']);
        $user->save();

        if ($request->has('role')) {
            $user = $this->repository->attachRole($request->get('role'), $user->id);
        }

        $token = JWTAuth::fromUser($user);

        if ($request->get('role') == 'user') {
            $user->status = User::STATUS_ACTIVE;
            $user->save();
            Auth::login($user);
        }

        // Event::fire(new UserRegisteredEvent($user));
        return $this->response->array(compact('token'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $user = $this->repository->find($id);

        if ($request->has('includes')) {
            $transformer = new UserTransformer(explode(',', $request->get('includes')));
        } else {
            $transformer = new UserTransformer;
        }

        return $this->response->item($user, $transformer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validator->isValid($request, 'RULE_UPDATE');
        $id = Auth::user()->id;

        $existsEmail = $this->repository->existsEmail($id, $request->get('email'));
        if ($existsEmail) {
            throw new ConflictHttpException("Email has exists", null, 1001);
        }
        $attributes = $request->all();

        $user = $this->repository->update($attributes, $id);

        return $this->response->item($user, new UserTransformer);
    }

    public function changePassword(Request $request)
    {
        $this->validator->isValid($request, 'CHANGE_PASSWORD');
        $user        = $this->repository->findByField('email', $request->get('email'))->first();
        $credentials = [
            'email'    => $request->get('email'),
            'password' => $request->get('old_password'),
        ];
        try {
            $token = JWTAuth::attempt($credentials);
            if (empty($token)) {
                throw new BadRequestHttpException("Old password does not match", null, 1048);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        $user->password = Hash::make($request->get('new_password'));
        $user->save();
        return $this->response->array(compact('token'));
    }

    public function images(Request $request, $id)
    {
        $this->validator->isValid($request, 'IMAGES');

        $user         = $this->repository->find($id);
        $user->avatar = $request->get('url');
        $user->save();
        return $this->success();
    }
}
