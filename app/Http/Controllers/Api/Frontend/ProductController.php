<?php

namespace App\Http\Controllers\Api\Frontend;

use App\Entities\Product;
use App\Exceptions\NotFoundException;
use App\Http\Controllers\Api\ApiController;
use App\Repositories\ProductRepository;
use App\Transformers\ProductTransformer;
use App\Validators\ProductValidator;
use Illuminate\Http\Request;

class ProductController extends ApiController
{

    private $repository;
    private $validator;

    public function __construct(ProductRepository $repository, ProductValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->middleware('jwt.auth', ['except' => ['index', 'show', 'getProductBySlug']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = new Product;

        $constraints = (array) json_decode($request->get('constraints'));
        if (count($constraints)) {
            $query = $query->where($constraints);
        }

        if ($request->has('search')) {
            $search = $request->get('search');
            $query  = $query->where('name', 'like', "%{$search}%");
        }

        if ($request->has('category_id')) {
            $category_id = $request->get('category_id');
            $query       = $query->whereHas('categories', function ($q) use ($category_id) {
                return $q->where('id', $category_id);
            });
        }

        if ($request->has('order_by')) {
            $orderBy = (array) json_decode($request->get('order_by'));
            if (count($orderBy) > 0) {
                foreach ($orderBy as $key => $value) {
                    $query = $query->orderBy($key, $value);
                }
            }
        } else {
            $query = $query->orderBy('id', 'desc');
        }

        $per_page = $request->has('per_page') ? (int) $request->get('per_page') : 15;
        $products = $query->paginate($per_page);

        if ($request->has('includes')) {
            $transformer = new ProductTransformer(explode(',', $request->get('includes')));
        } else {
            $transformer = new ProductTransformer;
        }
        return $this->response->paginator($products, $transformer);
    }

    /**
     * Get all items.
     *
     * @param  Request $request [Request].
     * @return \Illuminate\Http\Response
     */
    public function getAll(Request $request)
    {
        $query = new Product;

        $constraints = (array) json_decode($request->get('constraints'));
        if (count($constraints)) {
            $query = $query->where($constraints);
        }

        if ($request->has('search')) {
            $search = $request->get('search');
            $query  = $query->where('name', 'like', "%{$search}%");
        }

        if ($request->has('category_id')) {
            $category_id = $request->get('category_id');
            $query       = $query->whereHas('categories', function ($q) use ($category_id) {
                $q->where('id', $category_id);
            });
        }

        if ($request->has('order_by')) {
            $orderBy = (array) json_decode($request->get('order_by'));
            if (count($orderBy) > 0) {
                foreach ($orderBy as $key => $value) {
                    return $query = $query->orderBy($key, $value);
                }
            }
        } else {
            $query = $query->orderBy('id', 'desc');
        }

        $products = $query->get();

        if ($request->has('includes')) {
            $transformer = new ProductTransformer(explode(',', $request->get('includes')));
        } else {
            $transformer = new ProductTransformer;
        }
        return $this->response->collection($products, $transformer);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $product = $this->repository->find($id);
        if (!$product) {
            throw new NotFoundException('Product');
        }

        if ($request->has('includes')) {
            $transformer = new ProductTransformer(explode(',', $request->get('includes')));
        } else {
            $transformer = new ProductTransformer;
        }
        return $this->response->item($product, $transformer);
    }

    public function getProductBySlug($slug, Request $request)
    {
        $product = $this->repository->findByField('slug', $slug)->first();
        if (!$product) {
            throw new NotFoundException('Product');
        }

        if ($request->has('includes')) {
            $transformer = new ProductTransformer(explode(',', $request->get('includes')));
        } else {
            $transformer = new ProductTransformer;
        }
        return $this->response->item($product, $transformer);
    }
}
