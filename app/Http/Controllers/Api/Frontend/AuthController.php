<?php

namespace App\Http\Controllers\Api\Frontend;

use App\Entities\User;
use App\Exceptions\NotFoundException;
use App\Http\Controllers\Api\ApiController;
use App\Repositories\UserRepository;
use App\Transformers\UserTransformer;
use App\Validators\AuthValidator;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends ApiController
{
    private $repository;
    private $validator;

    public function __construct(UserRepository $repository, AuthValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');
        $user        = User::where('email', $credentials['email'])->where('status', User::STATUS_ACTIVE)->first();
        if (!$user) {
            throw new NotFoundException('Email');
        }

        if (!Hash::check($credentials['password'], $user->password)) {
            throw new Exception("Password does not match", 1003);
        }

        Auth::login($user);

        try {
            $token = JWTAuth::attempt($credentials);
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        return $this->response->array(compact('token'));
    }

    public function me(Request $request)
    {
        $user = Auth::user();

        if (!$user) {
            return response()->json([], 204);
        }

        Cache::flush();

        if ($request->has('includes')) {
            $transformer = new UserTransformer(explode(',', $request->get('includes')));
        } else {
            $transformer = new UserTransformer;
        }

        return $this->response->item($user, $transformer);
    }

    private function saveOrUpdateUser($social_account, $provider, $role, $user_id = null)
    {
        $name_arr    = explode(" ", $social_account->name);
        $name_length = count($name_arr);
        $first_name  = $name_arr[0];
        unset($name_arr[0]);
        $last_name = implode(' ', $name_arr);

        $gender = isset($social_account->user['gender']) && $social_account->user['gender'] == 'male' ? 1 : 0;

        if ($user_id != null) {
            $user = User::find($user_id);
        } else {
            $user               = new User;
            $user->account_type = $provider;
            $user->social_id    = $social_account->getId() ? $social_account->getId() : '';
            $user->first_name   = $first_name;
            $user->last_name    = $last_name;

            $user->email          = $social_account->getEmail() ? $social_account->getEmail() : $social_account->getId();
            $user->avatar         = $social_account->getAvatar() ? $social_account->getAvatar() : '';
            $user->gender         = $gender;
            $user->status         = User::STATUS_ACTIVE;
            $user->email_verified = User::EMAIL_VERIFIED;
            $user->save();
        }

        if ($role != null) {
            $user = $this->repository->attachRole($role, $user->id);
        }

        return $user;
    }

    public function socialLogin(Request $request)
    {
        $this->validator->isValid($request, 'SOCIAL_LOGIN');

        $provider       = $request->get('provider');
        $access_token   = $request->get('access_token');
        $social_account = Socialite::driver($provider)->userFromToken($access_token);

        $role = 'customer';

        $user             = null;
        $check_email_user = User::where('email', $social_account->getEmail())->first();
        if (!$check_email_user) {
            $check_user = User::where('account_type', $provider)->where('social_id', $social_account->getId())->first();
            if (!$check_user) {
                $user = $this->saveOrUpdateUser($social_account, $provider, $role);
            } else {
                $user = $this->saveOrUpdateUser($social_account, $provider, $role, $check_user['id']);
            }
        } else {
            $user = $this->saveOrUpdateUser($social_account, $provider, $role, $check_email_user['id']);
        }

        Auth::login($user);
        $token = JWTAuth::fromUser($user);
        return $this->response->array(compact('token'));
    }
}
