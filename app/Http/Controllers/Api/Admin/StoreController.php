<?php

namespace App\Http\Controllers\Api\Admin;

use App\Entities\Store;
use App\Exceptions\PermissionDeniedException;
use App\Http\Controllers\Api\ApiController;
use App\Repositories\StoreRepository;
use App\Transformers\StoreTransformer;
use App\Validators\StoreValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StoreController extends ApiController
{
    private $repository;
    private $validator;

    public function __construct(StoreRepository $repository, StoreValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->middleware('jwt.auth', ['except' => []]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = new Store;

        $constraints = (array) json_decode($request->get('constraints'));
        if (count($constraints)) {
            $query = $query->where($constraints);
        }

        if ($request->has('search')) {
            $search = $request->get('search');
            $query  = $query->where('name', 'like', "%{$search}%");
        }

        if ($request->has('order_by')) {
            $orderBy = (array) json_decode($request->get('order_by'));
            if (count($orderBy) > 0) {
                foreach ($orderBy as $key => $value) {
                    $query = $query->orderBy($key, $value);
                }
            }
        } else {
            $query = $query->orderBy('id', 'desc');
        }

        $per_page = $request->has('per_page') ? (int) $request->get('per_page') : 15;
        $stores   = $query->paginate($per_page);

        if ($request->has('includes')) {
            $transformer = new StoreTransformer(explode(',', $request->get('includes')));
        } else {
            $transformer = new StoreTransformer;
        }
        return $this->response->paginator($stores, $transformer);
    }

    /**
     * Get all items.
     *
     * @param  Request $request [Request].
     * @return \Illuminate\Http\Response
     */
    public function getAll(Request $request)
    {
        $query = new Store;

        $constraints = (array) json_decode($request->get('constraints'));
        if (count($constraints)) {
            $query = $query->where($constraints);
        }

        if ($request->has('search')) {
            $search = $request->get('search');
            $query  = $query->where('name', 'like', "%{$search}%");
        }

        if ($request->has('order_by')) {
            $orderBy = (array) json_decode($request->get('order_by'));
            if (count($orderBy) > 0) {
                foreach ($orderBy as $key => $value) {
                    $query = $query->orderBy($key, $value);
                }
            }
        } else {
            $query = $query->orderBy('id', 'desc');
        }

        $stores = $query->get();

        if ($request->has('includes')) {
            $transformer = new StoreTransformer(explode(',', $request->get('includes')));
        } else {
            $transformer = new StoreTransformer;
        }
        return $this->response->collection($stores, $transformer);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::user()->isRole('admin')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'RULE_CREATE');

        $data = $request->all();

        $store         = $this->repository->create($data);
        $store->status = Store::STATUS_ACTIVE;
        $store->save();

        return $this->response->item($store, new StoreTransformer);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $store = $this->repository->find($id);

        if ($request->has('includes')) {
            $transformer = new StoreTransformer(explode(',', $request->get('includes')));
        } else {
            $transformer = new StoreTransformer;
        }
        return $this->response->item($store, $transformer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Auth::user()->isRole('admin')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'RULE_UPDATE');

        $data  = $request->all();
        $store = $this->repository->update($data, $id);

        if ($request->has('status')) {
            $store->status = $request->get('status');
        }
        
        $store->save();

        return $this->response->item($store, new StoreTransformer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Auth::user()->isRole('admin')) {
            throw new PermissionDeniedException;
        }
        $this->repository->delete($id);
        return $this->success();
    }

    /**
     * Update all status resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function changeStatusAllItems(Request $request)
    {
        if (!Auth::user()->isRole('admin')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'CHANGE_STATUS_ALL_ITEMS');

        $data = $request->all();

        switch ($data['status']) {
            case 'active':
                Store::whereIn('id', $data['item_ids'])->update(['status' => Store::STATUS_ACTIVE]);
                break;
            case 'pending':
                Store::whereIn('id', $data['item_ids'])->update(['status' => Store::STATUS_PENDING]);
                break;
        }
        return $this->success();
    }

    /**
     * Update specified status resource in storage.
     *
     * @param  Request $request [Request].
     * @param  [type]  $id      [item id].
     * @return \Illuminate\Http\Response
     */
    public function changeStatusItem(Request $request, $id)
    {
        if (!Auth::user()->isRole('admin')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'CHANGE_STATUS_ITEM');

        $data = $request->all();

        switch ($data['status']) {
            case 'active':
                Store::where('id', $id)->update(['status' => Store::STATUS_ACTIVE]);
                break;
            case 'pending':
                Store::where('id', $id)->update(['status' => Store::STATUS_PENDING]);
                break;
        }
        return $this->success();
    }
}
