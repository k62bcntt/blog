<?php

namespace App\Http\Controllers\Api\Admin;

use App\Entities\User;
use App\Exceptions\PermissionDeniedException;
use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Api\FileController;
use App\Repositories\UserRepository;
use App\Transformers\UserTransformer;
use App\Validators\UserValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;

class UserController extends ApiController
{
    private $repository;
    private $validator;

    public function __construct(UserRepository $repository, UserValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->middleware('jwt.auth', ['except' => []]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = new User;

        if ($request->has('role')) {
            $query = $query->whereHas('roles', function ($q) use ($request) {
                if ($request->get('role') != '') {
                    $q->where('slug', $request->get('role'));
                }
            });
        }
        if ($request->has('roles')) {
            $query = $query->whereHas('roles', function ($q) use ($request) {
                $q->whereIn('slug', explode(',', $request->get('roles')));
            });
        }

        $constraints = (array) json_decode($request->get('constraints'));
        if (count($constraints)) {
            $query = $query->where($constraints);
        }

        if ($request->has('search')) {
            $search       = trim($request->get('search'));
            $search_array = explode(' ', $search);

            if (count($search_array) == 1) {
                $first_name = $search_array[0];
                $last_name  = '';
            }

            if (count($search_array) > 1) {
                $first_name = $search_array[0];

                $remain_array = $search_array;
                unset($remain_array[0]);

                $last_name = implode(" ", $remain_array);
            }

            if (isset($first_name) && $first_name != '') {
                $query = $query->where('first_name', 'like', "%{$first_name}%");

                if (isset($last_name) && $last_name != '') {
                    $query = $query->orWhere('last_name', 'like', "%{$last_name}%");
                } else {
                    $query = $query->orWhere('last_name', 'like', "%{$first_name}%");
                }
            }
        }

        if ($request->has('order_by')) {
            $orderBy = (array) json_decode($request->get('order_by'));
            if (count($orderBy) > 0) {
                foreach ($orderBy as $key => $value) {
                    $query = $query->orderBy($key, $value);
                }
            }
        } else {
            $query = $query->orderBy('id', 'desc');
        }

        $per_page = $request->has('per_page') ? (int) $request->get('per_page') : 15;
        $users    = $query->paginate($per_page);

        if ($request->has('includes')) {
            $transformer = new UserTransformer(explode(',', $request->get('includes')));
        } else {
            $transformer = new UserTransformer;
        }
        return $this->response->paginator($users, $transformer);
    }

    public function getAll(Request $request)
    {
        $query = new User;

        if ($request->has('role')) {
            $query = $query->whereHas('roles', function ($q) use ($request) {
                $q->where('slug', $request->get('role'));
            });
        }
        if ($request->has('roles')) {
            $query = $query->whereHas('roles', function ($q) use ($request) {
                $q->whereIn('slug', explode(',', $request->get('roles')));
            });
        }

        $constraints = (array) json_decode($request->get('constraints'));
        if (count($constraints)) {
            $query = $query->where($constraints);
        }

        if ($request->has('search')) {
            $search = $request->get('search');
            $query  = $query->where(function ($q) use ($request, $search) {
                $q->where('first_name', 'like', "%{$search}%");
                $q->orWhere('last_name', 'like', "%{$search}%");
            });
        }

        if ($request->has('order_by')) {
            $orderBy = (array) json_decode($request->get('order_by'));
            if (count($orderBy) > 0) {
                foreach ($orderBy as $key => $value) {
                    $query = $query->orderBy($key, $value);
                }
            }
        }
        $users = $query->get();

        if ($request->has('includes')) {
            $transformer = new UserTransformer(explode(',', $request->get('includes')));
        } else {
            $transformer = new UserTransformer;
        }
        return $this->collection($users, $transformer);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::user()->isRole('admin')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'ADMIN_CREATE_USER');
        if (!$this->repository->skipCache()->findByField('email', $request->get('email'))->isEmpty()) {
            throw new ConflictHttpException('Email already exist', null, 1001);
        }
        $data = $request->all();
        $user = $this->repository->create($data);

        if ($request->has('role')) {
            $user = $this->repository->attachRole($request->get('role'), $user->id);
        }

        $user->password       = Hash::make($data['password']);
        $user->email_verified = User::EMAIL_VERIFIED;
        $user->status         = User::STATUS_ACTIVE;
        $user->save();

        return $this->response->item($user, new UserTransformer);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $user = $this->repository->find($id);

        if ($request->has('includes')) {
            $transformer = new UserTransformer(explode(',', $request->get('includes')));
        } else {
            $transformer = new UserTransformer;
        }

        return $this->response->item($user, $transformer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Auth::user()->isRole('admin')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'ADMIN_UPDATE_USER');
        $existsEmail = $this->repository->existsEmail($id, $request->get('email'));
        if ($existsEmail) {
            throw new ConflictHttpException("Email has exists", null, 1001);
        }
        $attributes = $request->all();

        $user = $this->repository->update($attributes, $id);

        if ($request->has('status')) {
            $user->status = $request->get('status');
            $user->save();
        }

        return $this->response->item($user, new UserTransformer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Auth::user()->isRole('admin')) {
            throw new PermissionDeniedException;
        }
        $user = $this->repository->find($id);
        if ($user->isRole('admin') || $user->isRole('superadmin')) {
            throw new PermissionDeniedException;
        }

        if (filter_var($user->avatar, FILTER_VALIDATE_URL) !== false) {
            $domain = url('');
            $path   = str_replace($domain, '', $user->avatar);
        } else {
            $path = $user->avatar;
        }
        $delete_avatar = App::make(FileController::class)->delete($path);

        $this->repository->delete($id);
        return $this->success();
    }

    public function images(Request $request, $id)
    {
        if (!Auth::user()->isRole('admin')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'IMAGES');

        $user         = $this->repository->find($id);
        $user->avatar = $request->get('url');
        $user->save();
        return $this->success();
    }

    public function changeStatusAllItems(Request $request)
    {
        if (!Auth::user()->isRole('admin')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'CHANGE_STATUS_ALL_ITEMS');

        $data = $request->all();

        switch ($data['status']) {
            case 'active':
                User::whereIn('id', $data['item_ids'])->update(['status' => User::STATUS_ACTIVE]);
                break;
            case 'pending':
                User::whereIn('id', $data['item_ids'])->update(['status' => User::STATUS_PENDING]);
                break;
        }
        return $this->success();
    }

    public function changeStatusItem($id, Request $request)
    {
        if (!Auth::user()->isRole('admin')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'CHANGE_STATUS_ITEM');

        $data = $request->all();

        switch ($data['status']) {
            case 'active':
                User::where('id', $id)->update(['status' => User::STATUS_ACTIVE]);
                break;
            case 'pending':
                User::where('id', $id)->update(['status' => User::STATUS_PENDING]);
                break;
        }
        return $this->success();
    }
}
