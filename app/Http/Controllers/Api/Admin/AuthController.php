<?php

namespace App\Http\Controllers\Api\Admin;

use App\Exceptions\NotFoundException;
use App\Exceptions\PermissionDeniedException;
use App\Http\Controllers\Api\ApiController;
use App\Repositories\UserRepository;
use App\Transformers\UserTransformer;
use App\Validators\AuthValidator;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends ApiController
{
    private $repository;
    private $validator;

    public function __construct(UserRepository $repository, AuthValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            $user = $this->repository->findByField('email', $credentials['email'])->first();

            if (!$user) {
                throw new NotFoundException('Email');
            }

            if (!Hash::check($credentials['password'], $user->password)) {
                throw new Exception("Password does not match", 1003);
            }

            if ($user->isRole('admin')) {
                $token = JWTAuth::attempt($credentials);
            } else {
                throw new PermissionDeniedException;
            }
            
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        return $this->response->array(compact('token'));
    }

    public function me()
    {
        try {
            $user = JWTAuth::parseToken()->toUser();
            if (empty($user)) {
                throw new NotFoundException('User');
            }
            if (!$user->isRole('admin')) {
                throw new PermissionDeniedException;
            }
        } catch (Exception $e) {
            return response()->json([], 204);
        }

        Cache::flush();
        return $this->response->item($user, new UserTransformer);
    }
}
