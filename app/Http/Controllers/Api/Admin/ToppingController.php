<?php

namespace App\Http\Controllers\Api\Admin;

use App\Entities\Topping;
use App\Exceptions\PermissionDeniedException;
use App\Http\Controllers\Api\ApiController;
use App\Repositories\ToppingRepository;
use App\Transformers\ToppingTransformer;
use App\Validators\ToppingValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ToppingController extends ApiController
{

    private $repository;
    private $validator;

    public function __construct(ToppingRepository $repository, ToppingValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->middleware('jwt.auth', ['except' => []]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = new Topping;

        $constraints = (array) json_decode($request->get('constraints'));
        if (count($constraints)) {
            $query = $query->where($constraints);
        }

        if ($request->has('search')) {
            $search = $request->get('search');
            $query  = $query->where('name', 'like', "%{$search}%");
        }

        if ($request->has('order_by')) {
            $orderBy = (array) json_decode($request->get('order_by'));
            if (count($orderBy) > 0) {
                foreach ($orderBy as $key => $value) {
                    $query = $query->orderBy($key, $value);
                }
            }
        } else {
            $query = $query->orderBy('id', 'desc');
        }

        $per_page = $request->has('per_page') ? (int) $request->get('per_page') : 15;
        $toppings = $query->paginate($per_page);

        if ($request->has('includes')) {
            $transformer = new ToppingTransformer(explode(',', $request->get('includes')));
        } else {
            $transformer = new ToppingTransformer;
        }
        return $this->response->paginator($toppings, $transformer);
    }

    /**
     * Get all items.
     *
     * @param  Request $request [Request].
     * @return \Illuminate\Http\Response
     */
    public function getAll(Request $request)
    {
        $query = new Topping;

        $constraints = (array) json_decode($request->get('constraints'));
        if (count($constraints)) {
            $query = $query->where($constraints);
        }

        if ($request->has('search')) {
            $search = $request->get('search');
            $query  = $query->where('name', 'like', "%{$search}%");
        }

        if ($request->has('order_by')) {
            $orderBy = (array) json_decode($request->get('order_by'));
            if (count($orderBy) > 0) {
                foreach ($orderBy as $key => $value) {
                    $query = $query->orderBy($key, $value);
                }
            }
        } else {
            $query = $query->orderBy('id', 'desc');
        }

        $toppings = $query->get();

        if ($request->has('includes')) {
            $transformer = new ToppingTransformer(explode(',', $request->get('includes')));
        } else {
            $transformer = new ToppingTransformer;
        }
        return $this->response->collection($toppings, $transformer);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::user()->isRole('admin')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'RULE_CREATE');

        $data         = $request->all();
        $topping      = $this->repository->create($data);
        
        $topping->status = Topping::STATUS_ACTIVE;
        $topping->save();

        return $this->response->item($topping, new ToppingTransformer);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $topping = $this->repository->find($id);

        if ($request->has('includes')) {
            $transformer = new ToppingTransformer(explode(',', $request->get('includes')));
        } else {
            $transformer = new ToppingTransformer;
        }
        return $this->response->item($topping, $transformer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Auth::user()->isRole('admin')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'RULE_UPDATE');

        $data         = $request->all();
        $topping      = $this->repository->update($data, $id);

        if ($request->has('status')) {
            $topping->status = $request->get('status');
        }
        $topping->save();

        return $this->response->item($topping, new ToppingTransformer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Auth::user()->isRole('admin')) {
            throw new PermissionDeniedException;
        }
        $this->repository->delete($id);
        return $this->success();
    }

    /**
     * Update all status resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function changeStatusAllItems(Request $request)
    {
        if (!Auth::user()->isRole('admin')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'CHANGE_STATUS_ALL_ITEMS');

        $data = $request->all();

        switch ($data['status']) {
            case 'active':
                Topping::whereIn('id', $data['item_ids'])->update(['status' => Topping::STATUS_ACTIVE]);
                break;
            case 'pending':
                Topping::whereIn('id', $data['item_ids'])->update(['status' => Topping::STATUS_PENDING]);
                break;
        }
        return $this->success();
    }

    /**
     * Update specified status resource in storage.
     *
     * @param  Request $request [Request].
     * @param  [type]  $id      [item id].
     * @return \Illuminate\Http\Response
     */
    public function changeStatusItem(Request $request, $id)
    {
        if (!Auth::user()->isRole('admin')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'CHANGE_STATUS_ITEM');

        $data = $request->all();

        switch ($data['status']) {
            case 'active':
                Topping::where('id', $id)->update(['status' => Topping::STATUS_ACTIVE]);
                break;
            case 'pending':
                Topping::where('id', $id)->update(['status' => Topping::STATUS_PENDING]);
                break;
        }
        return $this->success();
    }
}
