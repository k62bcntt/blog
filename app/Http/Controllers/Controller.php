<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $jwt_token_key = 'jwt_token_key';
    protected $app_language  = 'app_language';

    const PROVINCE_TYPES = [
        [
            'key'   => 1,
            'value' => 'Thành phố',
        ],
        [
            'key'   => 2,
            'value' => 'Tỉnh',
        ],
    ];

}
