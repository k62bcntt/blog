<?php

namespace App\Providers;

use App\Repositories\CategoryRepository;
use App\Repositories\CategoryRepositoryEloquent;
use App\Repositories\PostRepository;
use App\Repositories\PostRepositoryEloquent;
use App\Repositories\ProductRepository;
use App\Repositories\ProductRepositoryEloquent;
use App\Repositories\StoreRepository;
use App\Repositories\StoreRepositoryEloquent;
use App\Repositories\ToppingRepository;
use App\Repositories\ToppingRepositoryEloquent;
use App\Repositories\UserRepository;
use App\Repositories\UserRepositoryEloquent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Request $request)
    {

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        App::bind(UserRepository::class, UserRepositoryEloquent::class);
        App::bind(StoreRepository::class, StoreRepositoryEloquent::class);
        App::bind(ProductRepository::class, ProductRepositoryEloquent::class);
        App::bind(ToppingRepository::class, ToppingRepositoryEloquent::class);
        App::bind(CategoryRepository::class, CategoryRepositoryEloquent::class);
        App::bind(PostRepository::class, PostRepositoryEloquent::class);
    }
}
