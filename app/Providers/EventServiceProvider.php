<?php

namespace App\Providers;

use App\Events\CustomerCommentOnRequirementEvent;
use App\Events\CustomerCreateRequirementEvent;
use App\Events\LawyerCommentOnRequirementEvent;
use App\Events\UpdateRatingForLawyerEvent;
use App\Events\UserChangeRequirementStatusEvent;
use App\Events\UserRegisteredEvent;
use App\Listeners\SendEmailToAdminWhenCustomerCreateRequirementListener;
use App\Listeners\SendEmailToAdminWhenUserChangeRequirementStatusListener;
use App\Listeners\SendEmailToCustomerAfterLawyerCommentOnRequirementtListener;
use App\Listeners\SendEmailToCustomerWhenCustomerCreateRequirementListener;
use App\Listeners\SendEmailToCustomerWhenLawyerChangeRequirementStatusListener;
use App\Listeners\SendEmailToLawyerAfterCustomerCommentOnRequirementtListener;
use App\Listeners\SendEmailToLawyerWhenCustomerChangeRequirementStatusListener;
use App\Listeners\SendEmailToLawyerWhenCustomerCreateRequirementListener;
use App\Listeners\UpdateRatingForLawyerListener;
use App\Listeners\UserRegisteredListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        UserRegisteredEvent::class               => [
            UserRegisteredListener::class,
        ],
        LawyerCommentOnRequirementEvent::class   => [

        ],
        UserChangeRequirementStatusEvent::class  => [
            SendEmailToAdminWhenUserChangeRequirementStatusListener::class,
            SendEmailToLawyerWhenCustomerChangeRequirementStatusListener::class,
            SendEmailToCustomerWhenLawyerChangeRequirementStatusListener::class,
        ],
        LawyerCommentOnRequirementEvent::class   => [
            SendEmailToCustomerAfterLawyerCommentOnRequirementtListener::class,
        ],
        CustomerCommentOnRequirementEvent::class => [
            SendEmailToLawyerAfterCustomerCommentOnRequirementtListener::class,
        ],
        CustomerCreateRequirementEvent::class    => [
            SendEmailToAdminWhenCustomerCreateRequirementListener::class,
            SendEmailToCustomerWhenCustomerCreateRequirementListener::class,
            SendEmailToLawyerWhenCustomerCreateRequirementListener::class,
        ],
        UpdateRatingForLawyerEvent::class => [
            UpdateRatingForLawyerListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
