<?php

namespace App\Mail;

class Mailable extends \Illuminate\Mail\Mailable
{

    public function view($view, array $data = [])
    {
        $this->view     = $view;
        $this->viewData = $data;

        return $this;
    }
}
