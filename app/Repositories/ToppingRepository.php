<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ToppingRepository
 * @package namespace App\Repositories;
 */
interface ToppingRepository extends RepositoryInterface
{
    
}
