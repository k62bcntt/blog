<?php

namespace App\Repositories;

use App\Entities\Topping;
use App\Repositories\CanFlushCache;
use App\Repositories\ToppingRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class ToppingRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ToppingRepositoryEloquent extends BaseRepository implements ToppingRepository, CacheableInterface
{
    use CacheableRepository, CanFlushCache;
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Topping::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
