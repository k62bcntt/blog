<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface StoreRepository
 * @package namespace App\Repositories;
 */
interface StoreRepository extends RepositoryInterface
{
    
}
