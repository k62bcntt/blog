<?php

namespace App\Repositories;

use App\Entities\User;
use App\Repositories\CanFlushCache;
use App\Repositories\UserRepository;
use NF\Roles\Models\Role;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class UserRepositoryEloquent
 * @package namespace App\Repositories;
 */
class UserRepositoryEloquent extends BaseRepository implements UserRepository, CacheableInterface
{
    use CacheableRepository, CanFlushCache;
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function existsEmail($id, $email)
    {
        $user = $this->findWhere([
            'email' => $email,
            ['id', '!=', $id],
        ])->first();
        if (!$user) {
            return false;
        } else {
            return $user;
        }
    }

    public function attachRole($role, $id)
    {
        $user = $this->find($id);
        if ($user->roles->isEmpty()) {
            $role = Role::where('slug', $role)->first();
            $user->attachRole($role);
            // throw new AccessDeniedHttpException("can not change the role of user", null, 1027);
        }
        return $user;
    }

    public function verifyEmail($user)
    {
        $user->email_verified = 1;
        $user->save();
        $this->flushCache();
        return $user;
    }
}
