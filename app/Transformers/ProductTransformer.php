<?php

namespace App\Transformers;

use App\Entities\Product;
use League\Fractal\TransformerAbstract;

/**
 * Class ProductTransformer
 * @package namespace App\Transformers;
 */
class ProductTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'categories',
        'orders',
    ];

    protected $commentTransformerDefaultIncludes = [];

    public function __construct($includes = [])
    {
        $this->setDefaultIncludes($includes);
    }

    /**
     * Transform the \Product entity
     * @param \Product $model
     *
     * @return array
     */
    public function transform(Product $model)
    {
        return [
            'id'          => (int) $model->id,
            'name'        => $model->name,
            'slug'        => $model->slug,
            'image'       => $model->image,
            'description' => $model->description,
            'size'        => (int) $model->size,
            'price'       => (int) $model->price,
            'sale_price'  => (int) $model->sale_price,
            'status'      => (int) $model->status,
            'order'       => (int) $model->order,
            'created_at'  => $model->created_at,
            'updated_at'  => $model->updated_at,
        ];
    }

    public function includeCategories(Product $model)
    {
        if (!empty($model->categories)) {
            return $this->collection($model->categories, new CategoryTransformer);
        }
    }
}
