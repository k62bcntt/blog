<?php

namespace App\Transformers;

use App\Entities\Topping;
use League\Fractal\TransformerAbstract;

/**
 * Class ToppingTransformer
 * @package namespace App\Transformers;
 */
class ToppingTransformer extends TransformerAbstract
{
    protected $availableIncludes = [];

    protected $commentTransformerDefaultIncludes = [];

    public function __construct($includes = [])
    {
        $this->setDefaultIncludes($includes);
    }

    /**
     * Transform the \Topping entity
     * @param \Topping $model
     *
     * @return array
     */
    public function transform(Topping $model)
    {
        return [
            'id'         => (int) $model->id,
            'name'       => $model->name,
            'price'      => (int) $model->price,
            'status'     => (int) $model->status,
            'order'      => (int) $model->order,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at,
        ];
    }
}
