<?php

namespace App\Transformers;

use App\Entities\Store;
use App\Transformers\UserTransformer;
use League\Fractal\TransformerAbstract;

/**
 * Class StoreTransformer
 * @package namespace App\Transformers;
 */
class StoreTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'user',
        'orders',
    ];

    protected $commentTransformerDefaultIncludes = [];

    public function __construct($includes = [])
    {
        $this->setDefaultIncludes($includes);
    }

    /**
     * Transform the \Store entity
     * @param \Store $model
     *
     * @return array
     */
    public function transform(Store $model)
    {
        return [
            'id'           => (int) $model->id,
            'name'         => $model->name,
            'address'      => $model->address,
            'location'     => $model->location,
            'phone_number' => $model->phone_number,
            'user_id'      => (int) $model->user_id,
            'status'       => (int) $model->status,
            'order'        => (int) $model->order,
            'created_at'   => $model->created_at,
            'updated_at'   => $model->updated_at,
        ];
    }

    public function includeUser(Store $model)
    {
        if (!empty($model->user)) {
            return $this->item($model->user, new UserTransformer);
        }
    }
}
